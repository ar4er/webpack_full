import "../index.html";
import "../styles/index.scss";
import code from "../img/fork_app.png";
import { sum, mult } from "./modules/calc";

console.log(sum(1, 2));
console.log(mult(1, 2));

const image = new Image();
image.src = code;

document.body.append(image)

console.log(123);